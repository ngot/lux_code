enum SearchAlgorithm {
  TEXT = 'TEXT',
  SOUNDEX = 'SOUNDEX'
}

export { SearchAlgorithm }
