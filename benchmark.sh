#!/bin/bash

export NODE_ENV=production
export EGG_SERVER_ENV=prod
export SEARCH_ALGORITHM=SOUNDEX # option:  SOUNDEX | TEXT

echo "starting server..."
{
  npm run clean
  npm run tsc
  npm stop
  npm start
} >/dev/null
echo "start sucesseed!"

echo ""
echo "benchmark without gzip..."
echo ""
curl -I -s http://127.0.0.1:7001/?q=Thailand >/dev/null # preheat server
wrk -c50 -t10 -d10 --latency http://127.0.0.1:7001/?q=Thailand

echo ""
echo "benchmark with gzip..."
echo ""
curl -H "Accept-Encoding: gzip" -I -s http://127.0.0.1:7001/?q=Thailand >/dev/null # preheat server
wrk -H "Accept-Encoding: gzip" -c50 -t10 -d10 --latency http://127.0.0.1:7001/?q=Thailand

echo ""
echo "cleaning..."
{
  npm stop
  npm run clean
} >/dev/null
echo "done"
