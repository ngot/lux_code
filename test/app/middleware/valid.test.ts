import { app, mock } from 'egg-mock/bootstrap';

describe('test/app/middleware/valid.test.ts', () => {
  before(async () => {
    await app.ready();
  });

  afterEach(() => mock.restore());

  it('should 422 when GET without q querystring', async () => {
    await app.httpRequest()
      .get('/')
      .expect(422);
  });
});
