import * as assert from 'assert';
import { Context } from 'egg';
import { app } from 'egg-mock/bootstrap';

describe('test/app/extend/helper.test.ts', () => {
  let ctx: Context;

  before(async () => {
    await app.ready();
    ctx = app.mockContext()
  });

  const equalCases = [
    ['baly', 'bali'],
    ['BalY', 'bAli'],
    ['Thailand', 'thailant'],
    ['Victoria', 'vyctoria'],
    ['Phuket', 'Phoket'],
  ];

  const notEqualCases = [
    ['baly', 'tali'],
    ['BatY', 'bAli'],
    ['Thailand', 'Phailant'],
    ['Victoria', 'vicforia'],
    ['Khuket', 'Phuket'],
  ];

  it('should soundex equal', () => {
    for (const t of equalCases) {
      assert(
        ctx.helper.soundex(t[0]) === ctx.helper.soundex(t[1]),
        `'${t[0]}' should soundex equal to '${t[1]}'`);
    }
  });

  it('should not soundex equal', () => {
    for (const t of notEqualCases) {
      assert(
        ctx.helper.soundex(t[0]) !== ctx.helper.soundex(t[1]),
        `'${t[0]}' should not soundex equal to '${t[1]}'`);
    }
  });
});
