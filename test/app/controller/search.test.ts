import * as assert from 'assert';
import * as path from 'path';
import { SearchAlgorithm } from '../../../lib/common';
import { app, mock } from 'egg-mock/bootstrap';
import { fs } from 'mz';
import * as rimraf from 'rimraf';
import * as makeDir from "make-dir";

describe('test/app/controller/search.test.ts', () => {
  before(async () => {
    rimraf.sync(app.config.tripOffersCachePath);
    const tripOffersDir: string = path.dirname(app.config.tripOffersCachePath);
    if (!await fs.exists(tripOffersDir)) {
      await makeDir(tripOffersDir);
    }
    await app.ready();
  });

  beforeEach(() => {
    app.mockService('syncTripOffers', 'sync', async () => {
      await fs.copyFile(path.join(__dirname, '../../fixtures/tripOffers.json'),
        app.config.tripOffersCachePath);
    });
    (app as any).lru.reset();
  });

  afterEach(() => mock.restore());

  it('should GET /?q=Thailand ok by TEXT Search', async () => {
    mock(app.config, 'searchAlgorithm', SearchAlgorithm.TEXT);
    await app.httpRequest()
      .get('/?q=Thailand')
      .expect({
        total: 1,
        keyword: 'Thailand',
        searchAlgorithm: 'TEXT',
        results:
          [{
            name: 'Thailand Ultimate Beachfront Haven With Brand New Aqua Wing',
            location: 'The Sands Khao Lak by Katathani, Thailand'
          }]
      })
      .expect(200);
  });

  it('should GET /?q=Thailand ok by SOUNDEX Search', async () => {
    mock(app.config, 'searchAlgorithm', SearchAlgorithm.SOUNDEX);
    await app.httpRequest()
      .get('/?q=Thailand')
      .expect({
        total: 2,
        keyword: 'Thailand',
        searchAlgorithm: 'SOUNDEX',
        results:
          [{
            "name": "Thailand Ultimate Beachfront Haven With Brand New Aqua Wing",
            "location": "The Sands Khao Lak by Katathani, Thailand"
          },
          {
            "name": "thailand Ultimate Beachfront Haven With Brand New Aqua Wing",
            "location": "The Sands Khao Lak by Katathani, thailand"
          },]
      })
      .expect(200);
  });

  it('should GET /?q=Thailand without gzip and from memory cache ok', async () => {
    mock(app.config, 'searchAlgorithm', SearchAlgorithm.TEXT);
    let result = await app.httpRequest()
      .get('/?q=Thailand')
      .set('accept-encoding', '')
      .expect(200);

    assert(result.headers['x-cached'] === 'MISS');
    assert(result.headers['content-encoding'] === undefined);

    result = await app.httpRequest()
      .get('/?q=Thailand')
      .set('accept-encoding', '')
      .expect(200);

    assert(result.headers['x-cached'] === 'HIT');
    assert(result.headers['content-encoding'] === undefined);
  });

  it('should GET /?q=Thailand with gzip and from memory cache ok', async () => {
    mock(app.config, 'searchAlgorithm', SearchAlgorithm.TEXT);
    let result = await app.httpRequest()
      .get('/?q=Thailand')
      .set('accept-encoding', 'gzip')
      .expect(200);

    assert(result.headers['x-cached'] === 'MISS');
    assert(result.headers['content-encoding'] === 'gzip');

    result = await app.httpRequest()
      .get('/?q=Thailand')
      .set('accept-encoding', 'gzip')
      .expect(200);

    assert(result.headers['x-cached'] === 'HIT');
    assert(result.headers['content-encoding'] === 'gzip');
  });

  async function etagTest(gzip?) {
    const tag = 'W/\"c3-RykWlNKqknqb4MSqUjhkGo9KSX0\"';
    const gzipCode = gzip ? 'gzip' : '';
    const contentEncoding = gzip ? 'gzip' : undefined;

    mock(app.config, 'searchAlgorithm', SearchAlgorithm.TEXT);

    let result = await app.httpRequest()
      .get('/?q=Thailand')
      .set('accept-encoding', gzipCode)
      .expect(200);

    assert(result.headers['x-cached'] === 'MISS');
    assert(result.headers['content-encoding'] === contentEncoding);
    assert(result.headers.etag === tag);

    result = await app.httpRequest()
      .get('/?q=Thailand')
      .set('accept-encoding', gzipCode)
      .expect(200);

    assert(result.headers['x-cached'] === 'HIT');
    assert(result.headers['content-encoding'] === contentEncoding);
    assert(result.headers.etag === tag);

    result = await app.httpRequest()
      .get('/?q=Thailand')
      .set('accept-encoding', gzipCode)
      .set('If-None-Match', tag)
      .expect(304);

    assert(result.headers['x-cached'] === 'HIT');
    assert(result.headers['content-encoding'] === contentEncoding);
    assert(result.headers.etag === tag);
  }

  it('should GET /?q=Thailand with/without gzip, with etag and from memory cache ok', async () => {
    await etagTest('gzip');
    await etagTest();
  });

  it('should GET /?q=Vietnam by location ok', async () => {
    mock(app.config, 'searchAlgorithm', SearchAlgorithm.TEXT);
    await app.httpRequest()
      .get('/?q=Vietnam')
      .expect({
        total: 2,
        keyword: 'Vietnam',
        searchAlgorithm: 'TEXT',
        results:
          [{
            name: 'High-End Vietnamese Beachfront Luxury Near Hoi An',
            location: 'Naman Retreat, Vietnam'
          },
          {
            name: 'Pan Pacific Hanoi: Club Access and Water Views',
            location: 'Vietnam'
          }]
      })
      .expect(200);
  });
});
