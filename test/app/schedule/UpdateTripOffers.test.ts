import * as assert from 'assert';
import * as ms from 'ms';
import * as rimraf from 'rimraf';
import * as path from 'path';
import { fs } from 'mz';
import { mock, app } from 'egg-mock/bootstrap';

describe('test/app/schedule/UpdateTripOffers.test.ts', function () {
  this.timeout(ms('2m'));
  before(async () => {
    await app.ready();
    rimraf.sync(path.dirname(app.config.tripOffersCachePath));
    await app.runSchedule('UpdateTripOffers');
  });

  after(() => mock.restore());

  it('sync', async () => {
    assert(await fs.exists(app.config.tripOffersCachePath));

    let offers = await fs.readFile(app.config.tripOffersCachePath);
    offers = JSON.parse(offers);
    assert(Array.isArray(offers));
    assert(offers.length > 0);

    for (const offer of offers) {
      assert(offer.name);
      assert(offer.location);
    }
  });
});
