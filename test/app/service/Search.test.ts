import * as assert from 'assert';
import { Context } from 'egg';
import { app, mock } from 'egg-mock/bootstrap';
import { SearchAlgorithm } from "../../../lib/common";

describe('test/app/service/Search.test.ts', () => {
  let ctx: Context;

  before(async () => {
    await app.ready();
    ctx = app.mockContext()
  });

  after(() => mock.restore());

  const textMatchSearchCases = [
    // target  keyword
    ['abc', 'b'],
    ['xyz', 'z'],
    ['mnk', 'm'],
  ];

  const soundexMatchCases = [
    // target  keyword
    ['Thailand Ultimate Beachfront Haven With Brand New Aqua Wing', 'Toailand'],
    ['Thailand Ultimate Beachfront Haven With Brand New Aqua Wing', 'thailand'],
    ['Pan Pacific Hanoi: Club Access and Water Views, Vietnam', 'vietnam'],
  ];

  it('searchByTextMatch', () => {
    for (const t of textMatchSearchCases) {
      assert(ctx.service.search.searchByTextMatch(t[0], t[1]),
        `'${t[1]}' should be found in '${t[0]}'`);
    }
  });

  it('searchBySoundexMatch', () => {
    for (const t of soundexMatchCases) {
      assert(ctx.service.search.searchBySoundexMatch(t[0], t[1]),
        `'${t[1]}' should be found in '${t[0]}'`);
    }
  });

  it('search', () => {
    mock(app.config, 'searchAlgorithm', SearchAlgorithm.TEXT);
    for (const t of textMatchSearchCases) {
      assert(ctx.service.search.search(t[0], t[1]),
        `'${t[1]}' should be found in '${t[0]}'`);
    }

    mock(app.config, 'searchAlgorithm', SearchAlgorithm.SOUNDEX);
    for (const t of soundexMatchCases) {
      assert(ctx.service.search.search(t[0], t[1]),
        `'${t[1]}' should be found in '${t[0]}'`);
    }
  });
});
