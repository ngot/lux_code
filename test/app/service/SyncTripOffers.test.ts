import * as assert from 'assert';
import * as ms from 'ms';
import * as rimraf from 'rimraf';
import { Context } from 'egg';
import { fs } from 'mz';
import { app, mock } from 'egg-mock/bootstrap';

describe('test/app/service/SyncTripOffers.test.ts', function () {
  this.timeout(ms('2m'));
  let ctx: Context;

  before(async () => {
    await app.ready();
    ctx = app.mockContext()
    rimraf.sync(app.config.tripOffersCachePath);
    await ctx.service.syncTripOffers.sync();
  });

  after(() => mock.restore());

  it('sync', async () => {
    assert(await fs.exists(app.config.tripOffersCachePath));

    let offers = await fs.readFile(app.config.tripOffersCachePath);
    offers = JSON.parse(offers);
    assert(Array.isArray(offers));
    assert(offers.length > 0);

    for (const offer of offers) {
      assert(offer.name);
      assert(offer.location);
    }
  });
});
