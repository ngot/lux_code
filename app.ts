import { Application } from 'egg';
import { fs } from 'mz';

export default (app: Application) => {
  /* istanbul ignore next */
  if (app.config.env !== 'unittest') {
    app.messenger.once('initTripOffer', () => {
      app.runInBackground(async () => {
        app.logger.info('TripOffers cache not found, start fetching, please wait a moment.');
        await app.runSchedule('UpdateTripOffers');
        app.logger.info('TripOffers fetch succeed!');
      });
    });
  }

  const tripOfferCache: any = {};

  (app as any).tripOfferCache = async () => {
    if (!tripOfferCache.cache) {
      if (!await fs.exists(app.config.tripOffersCachePath)) {
        await app.runSchedule('UpdateTripOffers');
      }
      const offer = await fs.readFile(app.config.tripOffersCachePath);
      tripOfferCache.cache = JSON.parse(offer);
      app.logger.debug('tripOffer memory cache expired, read from file!');
    }
    app.logger.debug('get tripOffer from memory cache!');
    return tripOfferCache.cache;
  }

  /* istanbul ignore next */
  app.messenger.on('updateTripOfferCache', () => {
    app.runInBackground(async () => {
      const offer = await fs.readFile(app.config.tripOffersCachePath);
      tripOfferCache.cache = JSON.parse(offer);
      app.logger.debug('update app worker tripOfferCache succeed!');
      (app as any).lru.reset();
      app.logger.debug('reset worker lru cache succeed!');
    });
  });
}
