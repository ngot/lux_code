import { Agent } from 'egg';
import { fs } from 'mz';

export default (agent: Agent) => {
  /* istanbul ignore next */
  if (agent.config.env !== 'unittest') {
    agent.messenger.once('egg-ready', () => {
      fs.exists(agent.config.tripOffersCachePath)
        .then((exist => {
          if (!exist) agent.messenger.sendRandom('initTripOffer');
        }));
    });
  }
}
