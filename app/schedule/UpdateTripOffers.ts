import { Application, Subscription } from 'egg';
import * as path from "path";
import * as makeDir from "make-dir";
import { fs } from 'mz';

export default (app: Application) => {
  class UpdateTripOffers extends Subscription {
    // schedule config
    static get schedule() {
      return {
        interval: app.config.updateInterval, // execute interval 
        type: 'worker', // only on worker will execute the schedule task
      };
    }

    // schedule execute entry
    async subscribe() {
      this.ctx.logger.debug('schedule update tripOffers start!');
      const tripOffersDir: string = path.dirname(app.config.tripOffersCachePath);
  
      if (!await fs.exists(tripOffersDir)) {
        this.ctx.logger.debug(`create dir: ${tripOffersDir}`);
        await makeDir(tripOffersDir);
      }

      await this.ctx.service.syncTripOffers.sync();
      app.messenger.sendToApp('updateTripOfferCache');
      this.ctx.logger.debug('schedule update tripOffers succeed!');
    }
  }

  return UpdateTripOffers;
}
