import { Controller } from 'egg';

interface TripOffer {
  name: string;
  location: string;
}

export default class SearchController extends Controller {
  public async index() {
    const { ctx } = this;
    const q: string = ctx.query.q;
    const offers: any = await (ctx.app as any).tripOfferCache();

    let results: Array<TripOffer> = offers;
    results = results.filter(d => {
      const { name, location } = d;
      if (ctx.service.search.search(name, q)) {
        return true;
      }

      if (ctx.service.search.search(location, q)) {
        return true;
      }

      return false;
    });

    ctx.body = {
      total: results.length,
      keyword: q,
      searchAlgorithm: ctx.app.config.searchAlgorithm,
      results,
    };
  }
}
