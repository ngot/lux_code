import { Service } from 'egg';

import { SearchAlgorithm } from "../../lib/common";

/**
 * Search Service
 */
export default class Search extends Service {
  /**
   * split the given string to words
   * @param text text string to split
   */
  private split(text: string): Array<string> {
    return text.split(/[\s,]+/);
  }

  /**
   * Search keyword in the text. If found, return true.
   * @param text -  search target
   * @param keyword - search keyword
   */
  public search(text: string, keyword: string): boolean {
    switch (this.ctx.app.config.searchAlgorithm) {
      case SearchAlgorithm.SOUNDEX:
        this.ctx.logger.debug(`search text by ${SearchAlgorithm.SOUNDEX} algorithm!`);
        return this.searchBySoundexMatch(text, keyword);
      default:
        this.ctx.logger.debug(`search text by ${SearchAlgorithm.TEXT} algorithm!`);
        return this.searchByTextMatch(text, keyword);
    }
  }

  /**
   * Search by soundex match
   * @param text -  search target
   * @param keyword - search keyword
   */
  public searchBySoundexMatch(text: string, keyword: string): boolean {
    const splits: Array<string> = this.split(text);
    const keywordSoundex = this.ctx.helper.soundex(keyword);
    return splits.some(word => keywordSoundex === this.ctx.helper.soundex(word));
  }

  /**
   * Search by text match
   * @param text -  search target
   * @param keyword - search keyword
   */
  public searchByTextMatch(text: string, keyword: string): boolean {
    return text.indexOf(keyword) > -1;
  }
}
