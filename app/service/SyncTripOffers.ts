import { Service } from 'egg';
import * as ms from "ms";

interface TripHref {
  href: string;
}

interface TripLink {
  self: TripHref;
  next?: TripHref;
  prev?: TripHref;
}

interface TripAPIDefine {
  total: number;
  status: number;
  count: number;
  message?: string;
  result: Array<any>;
  _links: TripLink;
}

/**
 * SyncTripOffers Service
 */
export default class SyncTripOffers extends Service {
  /**
   * Sync tripoffer with the server
   */
  public async sync() {
    let tripOffers: Array<any> = [];
    let tmp: TripAPIDefine | undefined;
    let url: string = this.ctx.app.config.tripOfferUrl;
    this.ctx.logger.debug('SyncTripOffers start!');

    while (true) {
      tmp = await this.request(url);
      if (tmp.result && Array.isArray(tmp.result) && tmp.result.length > 0) {
        tripOffers.push.apply(tripOffers, tmp.result);
      }

      if (tmp._links.next) {
        url = this.ctx.app.config.tripOfferHost + tmp._links.next.href;
      } else {
        break;
      }
    }

    await this.ctx.helper.writeFileAtomic(this.ctx.app.config.tripOffersCachePath,
      JSON.stringify(tripOffers));

    this.ctx.logger.debug('SyncTripOffers succeed!');
  }

  /**
   * 
   * @param url
   */
  private async request(url: string): Promise<TripAPIDefine> {
    this.ctx.logger.debug('SyncTripOffers request start:', url);
    const result = await this.ctx.curl(url,
      {
        method: 'GET',
        dataType: 'json',
        headers: {
          'Accept-Encoding': 'gzip',
        },
        timeout: ms('30s'),
      });
    this.ctx.logger.debug('SyncTripOffers request end:', url);
    return result.data;
  }
}
