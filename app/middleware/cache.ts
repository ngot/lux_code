import { Context } from 'egg';
import { gzip } from "zlib";
import { promisify } from "util";
import * as isJSON from "koa-is-json";
import * as etag from "etag";

export default () => {
  const pGzip = promisify(gzip);

  return async function cache(ctx: Context, next: any) {
    const q: string = ctx.query.q;
    const acceptsGzip = ctx.acceptsEncodings('gzip');
    /**
     * Get query result from memory.
     * Another choice is put the cache into Redis server.
     */
    const key = acceptsGzip ? `gzip_${q}` : `nozip_${q}`;
    if ((ctx.app as any).lru.has(key)) {
      const cache = (ctx.app as any).lru.peek(key);
      ctx.body = cache.body;
      ctx.type = 'json';
      ctx.set('x-cached', 'HIT');
      ctx.set('age', String((Date.now() - cache.timestamp) / 1000 | 0));
      ctx.set('etag', cache.etag);
      if (acceptsGzip) {
        ctx.set('content-encoding', 'gzip');
      }
      ctx.logger.debug('get result from memory cache!');
      return;
    } else {
      ctx.set('x-cached', 'MISS');
    }

    await next();

    let body = ctx.body;
    if (isJSON(body)) body = JSON.stringify(body);

    const etagEncode = etag(body, { weak: true });
    ctx.set('etag', etagEncode);

    if (acceptsGzip) {
      body = await pGzip(Buffer.from(body));
      ctx.set('content-encoding', 'gzip');
      ctx.body = body;
    }

    (ctx.app as any).lru.set(key, {
      body,
      timestamp: Date.now(),
      etag: etagEncode,
    });
  };
};
