import { Context } from 'egg';

export default () => {
  return async function valid(ctx: Context, next: any) {
    const q: string = ctx.query.q;
    ctx.logger.debug('querystring:', q);
    if (!q) {
      ctx.throw(422, 'querystring params `q` must be exist!');
    }
    await next();
  }
};
