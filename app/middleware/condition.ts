import { Context } from 'egg';

export default () => {
  return async function condition(ctx: Context, next: any) {
    await next();
    if (ctx.fresh) {
      ctx.status = 304;
      ctx.body = null;
      ctx.logger.debug('304 response!');
    }
  };
};
