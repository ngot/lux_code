
import * as writeFileAtomicCb from "write-file-atomic";

export default {
  /**
   * soundex encode ref: https://en.wikipedia.org/wiki/Soundex
   * @param text
   */
  soundex(text: string): string {
    text = text.toLocaleUpperCase();
    const MAX: number = 4;
    const A: number = 'A'.charCodeAt(0);
    //                      ABCDEFGHIJKLMNOPQRSTUVWXYZ
    const VALUES: string = "01230120022455012623010202";

    let result: string = '';

    for (let i: number = 1; i < text.length && result.length <= MAX; i++) {
      const idx: number = text[i].charCodeAt(0) - A;
      if (VALUES[idx] !== '0' // remove a, e, i, o, u, y, h, w
        && VALUES[idx] !== undefined // remove none letter
        && result[result.length - 1] !== VALUES[idx] // ignore repeat code
      ) {
        result += VALUES[idx];
      }
    }

    if (result.length < MAX) { // append with zeros until MAX
      result += '0'.repeat(MAX - result.length);
    }

    return text[0] + result;
  },

  /**
   * write file async & atomic
   * @param filename 
   * @param data 
   */
  writeFileAtomic(filename, data): Promise<void> {
    return new Promise((resolve, reject) => {
      writeFileAtomicCb(filename, data, err => {
        /* istanbul ignore next */
        if (err) return reject(err);
        resolve();
      });
    });
  },
}
