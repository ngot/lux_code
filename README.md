# lux_code

[Lux Group Coding Test](https://gist.github.com/forkfork/048a6af6d42437bc580593074b51294d) Project

## Quick Start

Start server at local,

```bash
$npm i
$npm run dev
```

After started,

We can do q query,

```bash
$curl http://127.0.0.1:7001/?q=Thailand
```

or with gzip encoding support,

```bash
$curl --compressed -H"accept-encoding: gzip" http://127.0.0.1:7001/?q=Thailand
```

### Technical Choice

- [Egg.js](https://eggjs.org/)
  
    - Built-in cluster, a very powerful multi-process model.

        When an application starts up, 3 kinds of processes will be forked: Master, Agent and Worker.

        ![](./imgs/multi-process-model.png)

        Master process undertakes the process management workers(like pm2) but runs no business codes. We simply start up a Master process and it will handle all initialization and restarting issues of Worker and Agent processes.

        The agent process is in charge of tough and tedious work like keeping connections, which is very useful to work with distributed RPC microservices.

        Worker process runs business codes, which are more complicated than those of Agent and Master but the stability may be lower, a Worker process will be restarted by Master when a Worker process exits unexpectedly.

    - Difference with Express.js

        Express.js is widely adopted in Node.js community. It's easy to use and very extensible and fit personal project well. However, without a default convention, the standard MVC model has lots of wired implementation which leads to a lot of confusion. That's why IMO the Express is not a good choice for a real project that needs teamwork. By following convention-over-configuration, Egg.js reduces the cost of team collaboration to a significantly low level.

    - Relation with [Koa](https://github.com/koajs/koa)
  
        Egg.js is based on Koa.
   
        P.S. Koa is also maintained by several guys of the Egg.js team.

        It integrated with a lot of useful tools: Unit Test, Test Coverage, Development, Deployment, etc.

    - Practice on Production

        Egg.js has been proven robust by the world top traffic & complex situations(The Single Day Shopping Festival which has up to 1 million QPS). 

        It was designed & developed by myself and other team members together and has been open source by alibaba group for about 2 years.

- [TypeScript](https://www.typescriptlang.org/)

    - Type Safe language.

### Directory Structure

```
├── README.md
├── app
│   ├── controller
│   │   └── search.ts
│   ├── extend
│   │   └── helper.ts
│   ├── middleware
│   │   ├── cache.ts
│   │   ├── error_handler.ts
│   │   └── valid.ts
│   ├── router.ts
│   ├── schedule
│   │   └── UpdateTripOffers.ts
│   └── service
│       ├── Search.ts
│       └── SyncTripOffers.ts
├── app.ts
├── agent.ts
├── benchmark.sh
├── bitbucket-pipelines.yml
├── config
│   ├── config.default.ts
│   ├── config.local.ts
│   ├── config.prod.ts
│   └── plugin.ts
├── lib
│   └── common.ts
├── package.json
├── test
│   ├── app
│   │   ├── controller
│   │   │   └── search.test.ts
│   │   ├── extend
│   │   │   └── helper.test.ts
│   │   ├── middleware
│   │   │   └── valid.test.ts
│   │   ├── schedule
│   │   │   └── UpdateTripOffers.test.ts
│   │   └── service
│   │       ├── Search.test.ts
│   │       └── SyncTripOffers.test.ts
│   └── fixtures
│       └── tripOffers.json
├── tsconfig.json
└── tslint.json
```

### System Design

#### Facts and issues

- The trip offers api response is slow, it takes about 3s for the first page 25 results
- The response size is big, it's 1.2MB for the first page 25 results
- The _link contains next paging url
- The total size of the response is only 1.7MB currently
- Take `q=thailand` as an example, the response contains only three results, but size is about 160KB, which would use quite a traffic.
- The trip offers api is not updated frequently.

#### Design Goals

- The API should return very quickly.
- The server should have high QPS capability at a low hardware cost.
- High availability, works even when The trip offers api is down.

#### Solution

- Cache the trip offers results
    - Fetch the trip offers asynchronously in the background after starting the server.
    - If there is no trip offers cached at local, then fetch and saved it. After that continue the rest of process. This case happens only if the user does a search early before the async fetch finished.
    - Starting schedule task to update the trip offers cache in the background. If there are multiple processes, only one process will do this updating task.
- Compress the response
    - using `gzip` to compress the query response, and put it into `LRU-Cache`.
    - If the query keyword hit the `LRU-Cache`, then get the result from the `LRU` cache directly.
    - The scheduled task will also empty the `LRU-Cache` automatically after updating succeed.
- Etag support: calculate the Etag of the search result, and cached with the search result to get better performance.
- Search Algorithm
    - We can decide the search algorithm by exporting `SEARCH_ALGORITHM`, the option values are: `SOUNDEX` | `TEXT`, default to `TEXT`.
    - The query response body contains a field `searchAlgorithm`, tells us which algorithm adopted.
    - The soundex algorithm implemented by myself according to the definition on Wikipedia. The implementation code is located at `app/extend/helper.ts`.
    - The text match algorithm adopt the JavaScript build-in `String.prototype.indexOf`.

### Development

```bash
$ npm i
$ npm run dev
$ open http://localhost:7001/?q=thailand
```

### UnitTest

```bash
$ npm run test-local # run the unit test
$ npm test # run the unit test and eslint
```

Don't tsc compile at development mode, if you had run `tsc` then you need to `npm run clean` before `npm run dev`.

### Benchmark

We can run the benchmark script to do the benchmark of our service.

```
./benchmark.sh
```

After a while, we will get the report.

Below is a benchmark report from my laptop, `MacBook Pro (Retina, 15-inch, Mid 2015)`.

```
starting server...
start sucesseed!

benchmark without gzip...

Running 10s test @ http://127.0.0.1:7001/?q=Thailand
  10 threads and 50 connections
  Thread Stats   Avg      Stdev     Max   +/- Stdev
    Latency    22.55ms   38.27ms 599.03ms   96.63%
    Req/Sec   292.71     93.05   484.00     66.57%
  Latency Distribution
     50%   19.89ms
     75%   23.67ms
     90%   28.08ms
     99%  216.65ms
  28962 requests in 10.05s, 4.61GB read
Requests/sec:   2881.00
Transfer/sec:    469.25MB

benchmark with gzip...

Running 10s test @ http://127.0.0.1:7001/?q=Thailand
  10 threads and 50 connections
  Thread Stats   Avg      Stdev     Max   +/- Stdev
    Latency     3.42ms    3.99ms  98.16ms   92.67%
    Req/Sec     1.76k   448.90     2.80k    68.19%
  Latency Distribution
     50%    2.41ms
     75%    3.76ms
     90%    6.27ms
     99%   16.96ms
  176344 requests in 10.10s, 5.95GB read
Requests/sec:  17453.16
Transfer/sec:    602.65MB

cleaning...
done
```

### Deploy

```bash
$ npm run tsc
$ npm start
```

### Npm Scripts

- Use `npm run lint` to check code style
- Use `npm test` to run unit test
- Use `npm run clean` to clean compiled js at development mode once

### Requirement

- Node.js 8.x
- Typescript 2.8+
