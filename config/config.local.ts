import { DefaultConfig } from './config.default';

export default () => {
  const config: DefaultConfig = {};

  config.logger = {
    consoleLevel: process.env.EGG_DEBUG ? 'DEBUG' : 'INFO',
  };

  // In development mod, update tripOffers from remote server per minute.
  config.updateInterval = '1m';

  return config;
};
