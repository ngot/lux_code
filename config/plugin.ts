import { EggPlugin } from 'egg';

const plugin: EggPlugin = {
  schedule: {
    enable: true,
  },
  lru: {
    enable: true,
    package: 'egg-lru',
  }
};

export default plugin;
