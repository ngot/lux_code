import { EggAppConfig, EggAppInfo, PowerPartial } from 'egg';
import * as assert from "assert";
import * as path from "path";
import * as ms from "ms";

import { SearchAlgorithm } from "../lib/common";

// for config.{env}.ts
export type DefaultConfig = PowerPartial<EggAppConfig & BizConfig>;

// app special config scheme
export interface BizConfig {
  searchAlgorithm: string;
  tripOffersCachePath: string;
  tripOfferUrl: string;
  tripOfferHost: string;
  updateInterval: string;
}

export default (appInfo: EggAppInfo) => {
  const config = {} as PowerPartial<EggAppConfig> & BizConfig;

  // override config from framework / plugin
  // use for cookie sign key, should change to your own and keep security
  config.keys = appInfo.name + '_1525336456272_2382';

  // add your config here
  config.middleware = ['errorHandler', 'condition', 'valid', 'cache'];

  const searchAlgorithm = process.env.SEARCH_ALGORITHM || SearchAlgorithm.TEXT;
  if (searchAlgorithm) {
    assert(
      searchAlgorithm === SearchAlgorithm.TEXT
      || searchAlgorithm === SearchAlgorithm.SOUNDEX,
      `SEARCH_ALGORITHM must be \`${SearchAlgorithm.TEXT}\` or \`${SearchAlgorithm.SOUNDEX}\` or just leave it Undefined.`);
  }

  config.searchAlgorithm = searchAlgorithm;

  config.tripOffersCachePath = path.join(appInfo.root, 'tmp/tripOffers.json');

  config.tripOfferUrl = 'https://api.luxgroup.com/api/public-offers?page=1&limit=20';

  config.tripOfferHost = 'https://api.luxgroup.com';

  // In production mod, update tripOffers from remote server per hour.
  config.updateInterval = '1h';

  config.lru = {
    client: {
      max: 100,
      maxAge: ms('1h'), // 1 hour cache
    },
  };

  return config;
};
